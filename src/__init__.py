import markdown

from flask import Flask, make_response, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache
from werkzeug.exceptions import HTTPException

from .config import AppConfig


db = SQLAlchemy()
cache = Cache()


def create_app() -> Flask:
    app = Flask(__name__)

    app.config.from_object(
        AppConfig
    )

    if not app.config['SECRET_KEY']:
        raise ValueError('SECRET KEY must be set for your app')

    db.init_app(app)
    cache.init_app(app)

    from src import getobject
    from .config import api
    api.add_resource(
        getobject.Objects,
        '/storage/api/v0.1/objects',
        '/storage/api/v0.1/objects/<idx>'
    )

    api.init_app(app)

    @app.errorhandler(HTTPException)
    def handle_exception(error):
        response = make_response(jsonify({
            'error': {
                'code': error.code,
                'status': error.name,
                'description': error.description
            }
        }), error.code)
        response.headers['Content-Type'] = 'application/json'
        return response

    @app.route('/')
    def index() -> str:
        return render_template('index.html')

    @app.route('/tasks')
    def tasks() -> str:
        with open('./tasks/TASKS.md', encoding='UTF-8') as fd:
            html = markdown.markdown(fd.read())
        return html

    @app.route('/todo')
    def todo() -> str:
        with open('./tasks/TODO.md', encoding='UTF-8') as fd:
            html = markdown.markdown(fd.read())
        return html

    return app
