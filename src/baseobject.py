from typing import Optional

from sqlalchemy.orm import relationship, backref

from . import db


class BaseObject(db.Model):
    """Представляет данные записи из БД."""
    __tablename__ = 'objects2'

    objects2_id_seq = db.Sequence('objects2_id_seq')
    id = db.Column(db.Integer, objects2_id_seq, primary_key=True)
    name = db.Column(db.JSON, nullable=False)
    extension = db.Column(db.String(250))
    innertype = db.Column(db.String(250))
    parent_id = db.Column(
        db.Integer,
        db.ForeignKey('objects2.id'),
        nullable=False)
    children = relationship(
        'BaseObject',
        backref=backref('parent', remote_side=[id]))

    def __str__(self) -> str:
        return ', '.join(map(
            str,
            (
                self.id,
                self.name,
                self.extension,
                self.innertype,
                self.parent_id,
                str(self.children))
        ))

    def get_tree(self, language: str, filter_sequence: Optional[str]) -> dict:
        """Получает вид записи в виде словаря."""
        visible = (
            True
            if (
                filter_sequence is None or
                filter_sequence in self.name[language])
            else False)
        if visible or self.innertype == 'node':
            tree = dict(
                    id=self.id,
                    name=self.name[language],
                    extension=self.extension,
                    innertype=self.innertype,
                    visible=visible,
                    children={
                        obj.name[language]: branch for obj in self.children
                        if (branch := obj.get_tree(language, filter_sequence))
                    } if self.children else None
                )
            if not (self.innertype == 'node' and not tree['children']):
                return tree
            if self.innertype == 'node' and visible:
                return tree

    def get_view(self, language: str,
                 filter_sequence: Optional[str] = None) -> dict:
        return {self.name[language]: self.get_tree(language, filter_sequence)}

    def set_nextval(self, idx: int) -> str:
        return (f"ALTER SEQUENCE {self.objects2_id_seq.name}"
                f" RESTART WITH {idx+1}")
