from flask_httpauth import HTTPTokenAuth


auth = HTTPTokenAuth()

# Токены для примера (в реальности нужно хранить в БД)
tokens = {
    'secret-token-1': 'client-1',
    'secret-token-2': 'client-2',
}


@auth.verify_token
def verify_token(token):
    if token in tokens:
        return tokens[token]
