from flask import make_response, jsonify
from flask_restful import Api, reqparse

from dicttoxml import dicttoxml


errors = {
    'BadRequest': {
        'status': '400 BAD REQUEST',
        'code': 400
    },
    'Unauthorized': {
        'status': '401 UNAUTHORIZED',
        'code': 401
    },
    'NotFound': {
        'status': '404 NOT FOUND',
        'code': 404
    },
    'MethodNotAllowed': {
        'status': '405 METHOD NOT ALLOWED',
        'code': 405,
    },
}

api = Api(errors=errors)

@api.representation('application/xml')
def output_xml(data, code, headers=None):
    resp = make_response(dicttoxml(data), code)
    if headers:
        for key in headers.keys():
            if key in resp.headers:
                resp.headers.update(headers)
            else:
                resp.headers.extend(headers)
    return resp

@api.representation('application/json')
def output_json(data, code, headers=None):
    resp = make_response(jsonify(data), code)
    if headers:
        for key in headers.keys():
            if key in resp.headers:
                resp.headers.update(headers)
            else:
                resp.headers.extend(headers)
    return resp


parser = reqparse.RequestParser()
parser.add_argument('name', type=dict, location='json', required=True, help='Имя не может быть пустым!')
parser.add_argument('extension', type=str)
parser.add_argument('innertype', type=str)
parser.add_argument('parent_id', type=int, required=True, help="Объект должен добавляться к родительскому элементу.")