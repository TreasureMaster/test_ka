import os


# -------------------- Базовые настройки всего приложения -------------------- #
class BaseConfig:
    """Базовые настройки приложения."""
    # ключ
    SECRET_KEY = os.environ.get('SECRET_KEY')

    # вид возврата json файлов (сортировка)
    JSON_SORT_KEYS = False


# -------------------------- Настройки выбранных БД -------------------------- #
class PostgresDBConfig(BaseConfig):
    """Данные основной БД - Postgres"""
    POSTGRES_USER = os.environ.get('POSTGRES_USER')
    POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
    POSTGRES_DB = os.environ.get('POSTGRES_DB')
    POSTGRES_HOST = host if (host := os.environ.get('POSTGRES_HOST')) else 'localhost'
    # print(os.environ.get('POSTGRES_DB'))
    # postgresql+psycopg2://postgres:root@localhost/tests_orbis

    # настройка sqlalchemy
    SQLALCHEMY_DATABASE_URI = '{}{}:{}@{}/{}'.format(
    os.environ.get('SQLALCHEMY_DATABASE_PREFIX'),
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    POSTGRES_HOST,
    POSTGRES_DB
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


# ---------------------- Общие настройки приложения с БД --------------------- #
if (use_db := os.environ.get('FLASK_BASE_DB_TYPE')) == 'postgres':
    BaseDBConfig = PostgresDBConfig
else:
    ValueError(f'Environment contains incorrect data about DB: {use_db}')


# --------------------------- Настройки кеширования -------------------------- #
class RedisCacheConfig(BaseDBConfig):
    """Настройки с кешированием запросов в Redis."""
    # настройка кеша
    CACHE_TYPE = 'RedisCache'
    CACHE_REDIS_HOST = os.environ.get('CACHE_REDIS_HOST') or 'redis'
    CACHE_REDIS_PORT = 6379
    CACHE_REDIS_DB = 0
    CACHE_REDIS_URL = 'redis://{}:{}/{}'.format(
        CACHE_REDIS_HOST,
        CACHE_REDIS_PORT,
        CACHE_REDIS_DB
    )
    CACHE_DEFAULT_TIMEOUT = 500


class NullCacheConfig(BaseDBConfig):
    """Настройки без кеширования."""
    # настройка кеша
    CACHE_TYPE = 'NullCache'
    CACHE_DEFAULT_TIMEOUT = 500


# ------------------------ Полные настройки приложения ----------------------- #
if (use_cache := os.environ.get('FLASK_CACHE_CONFIG')) == 'redis':
    AppConfig = RedisCacheConfig
else:
    AppConfig = NullCacheConfig


# config_scheme = {
#     'null': NullCacheConfig,
#     'redis': RedisCacheConfig,

#     'default': NullCacheConfig
# }