from flask import request
from sqlalchemy import func
from sqlalchemy.sql.elements import literal
from flask_restful import Resource, abort
from typing import Any, Optional

from . import cache
from .dbi import get_session
from .baseobject import BaseObject
from .authentication import auth
from .config import parser, api


BASE_FORMATS = {
    None: 'application/json',
    'json': 'application/json',
    'xml': 'application/xml'
}
BASE_LANGUAGES = {'english', 'russian', 'deutsch'}
DEFAULT_LANGUAGE = 'english'


class Objects(Resource):

    @auth.login_required
    @cache.cached(timeout=30, query_string=True)
    def get(self, idx=1):
        # тест длительного запроса
        import time
        time.sleep(0.25)
        idx = self.check_index(idx, request.method)

        query_filter = request.args.get('filter')
        query_format = request.args.get('format')
        content_type = BASE_FORMATS.get(query_format)
        if content_type is None:
            abort(400,
                  description=(f"Запрошен недопустимый формат"
                               f" '{query_format}'."))

        language = request.args.get('lng')
        if language is None:
            language = 'english'
        elif (language := language.lower()) not in BASE_LANGUAGES:
            abort(400,
                  description=f"Запрошен недопустимый язык: '{language}'.")

        session = get_session()

        s = session.query(BaseObject).filter(
            BaseObject.id == idx
        )

        data = s.all()
        if not data:
            abort(404, description=f"Объект с id={idx} не существует.")

        data = data[0].get_view(language, query_filter)

        resp = api.representations[content_type](
            data,
            200,
            {'Content-Type': content_type}
        )

        return resp

    @auth.login_required
    def post(self, idx=None):
        idx = self.check_index(idx, request.method)
        if not request.json:
            abort(400, description="Создание записи поддерживает только json.")
        args = parser.parse_args()

        session = get_session()

        parent = session.query(BaseObject.innertype).filter(
            BaseObject.id == args['parent_id']
        ).first()
        if not parent:
            abort(400,
                  description=(f"Не существует родительский элемент"
                               f" с id={args['parent_id']}"))
        if parent.innertype != 'node':
            abort(400,
                  description=(f"Невозможно присоединить новый элемент"
                               f" к родительскому элементу с типом"
                               f" '{parent.innertype}'"))

        languages = self.check_languages(args['name'])

        idx_exists = session.query(func.exists(
            session.query(literal(1)).filter(BaseObject.id == idx)
        )).scalar()

        if not idx_exists:
            new_object = BaseObject(
                id=idx,
                name=languages,
                extension=args['extension'],
                innertype=args['innertype'],
                parent_id=args['parent_id']
            )
            session.add(new_object)
            if idx:
                session.execute(new_object.set_nextval(idx))
            cache.clear()
            session.commit()

        else:
            abort(400,
                  description=(f"Невозможно добавить запись:"
                               f" id={idx} уже существует."))

        return api.representations['application/json'](
            new_object.get_view(DEFAULT_LANGUAGE),
            201,
            {'Content-Type': 'application/json'}
        )

    @auth.login_required
    def put(self, idx=None):
        idx = self.check_index(idx, request.method)
        if not request.json:
            abort(400,
                  description="Обновление записи поддерживает только json.")
        args = parser.parse_args()

        session = get_session()

        parent = session.query(BaseObject.innertype).filter(
            BaseObject.id == args['parent_id']
        ).first()
        if not parent:
            abort(400,
                  description=(f"Не существует родительский элемент"
                               f" с id={args['parent_id']}"))
        if parent.innertype != 'node':
            abort(400,
                  description=(f"Невозможно присоединить новый элемент к"
                               f" родительскому элементу"
                               f" с типом '{parent.innertype}'"))

        languages = self.check_languages(args['name'])

        idx_exists = session.query(func.exists(
            session.query(literal(1)).filter(BaseObject.id == idx)
        )).scalar()

        if idx_exists:
            updated_object = session.query(BaseObject).get(idx)
            if updated_object.children and args['innertype'] != 'node':
                abort(400,
                      description=(f"Невозможно изменить тип объекта c 'node'"
                                   f" на '{args['innertype']}',"
                                   f" если существуют дочерние объекты"))
            updated_object.name = languages
            updated_object.extension = args['extension']
            updated_object.innertype = args['innertype']
            updated_object.parent_id = args['parent_id']
            session.add(updated_object)
            cache.clear()
            session.commit()

        else:
            abort(400,
                  description=(f"Невозможно добавить запись:"
                               f" id={idx} еще не существует."))

        return api.representations['application/json'](
            session.query(BaseObject).get(idx).get_view(DEFAULT_LANGUAGE),
            200,
            {'Content-Type': 'application/json'}
        )

    @auth.login_required
    def delete(self, idx=None):
        idx = self.check_index(idx, request.method)

        session = get_session()

        idx_exists = session.query(func.exists(
            session.query(literal(1)).filter(BaseObject.id == idx)
        )).scalar()

        if idx_exists:
            delete_object = session.query(BaseObject).get(idx)
            if delete_object.children:
                abort(400,
                      description=("Невозможно удалить объекта типа 'node',"
                                   " если существуют дочерние объекты"))
            session.delete(delete_object)
            cache.clear()
            session.commit()

        else:
            abort(400,
                  description=(f"Невозможно удалить запись:"
                               f" id={idx} еще не существует."))

        return None, 204

    def check_index(self, idx: Any, request_method: str) -> Optional[int]:
        if request_method.upper() == 'POST' and idx is None:
            return idx
        try:
            idx = int(idx)
            if idx <= 0:
                raise ValueError
        except (ValueError, TypeError):
            abort(400,
                  description=(f"Идентификатор должен быть целым числом,"
                               f" большим 0. Задано: '{idx}'."))
        return idx

    def check_languages(self, languages: dict) -> dict:
        languages = {key.lower(): value for key, value in languages.items()}
        if set(languages.keys()) != BASE_LANGUAGES:
            abort(400,
                  description=(f"Новая запись должна содержать только все"
                               f" поддерживаемые языки:"
                               f" {', '.join(BASE_LANGUAGES)}"))
        return languages
