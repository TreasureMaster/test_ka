FROM python:3.8

# COPY deploy/ ./home/test_ka/deploy/
WORKDIR /home/test_ka
COPY . .
RUN pip3 install -r requirements.txt --no-cache-dir
CMD uwsgi --ini wsgi.ini

# RUN echo $(ls)
# WORKDIR /home/test_ka/deploy
# RUN echo $(ls -a)
# COPY . ./test_ka