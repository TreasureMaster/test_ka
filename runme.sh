#!/bin/bash

echo; echo "Выберите номер действия и нажмите Enter:"
echo "1. Запуск тестового проекта в режиме development."
echo "2. Запуск тестового проекта в режиме production."


while true; do
    read Keypress
    case "$Keypress" in
        1) docker-compose -f docker-compose-dev.yml up -d --build --remove-orphans; break;;
        2) docker-compose -f docker-compose.yml up -d --build; break;;
        exit) break ;;
        *) echo "выберите действие или exit";;
    esac
done