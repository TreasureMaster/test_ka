DROP TABLE IF EXISTS objects2;
-- CREATE SEQUENCE objects2_id_seq;

CREATE TABLE objects2(
    id serial PRIMARY KEY NOT NULL,
    name json NOT NULL,
    extension character varying,
    innertype character varying,
    parent_id integer NOT NULL
);

INSERT INTO objects2 (id,name,"extension",innertype,parent_id) VALUES
	 (3,'{"english": "main commands", "russian": "базовые команды", "deutsch": "Hauptbefehle"}','py','common',2),
	 (4,'{"english": "init", "russian": "инициализация", "deutsch": "Initial"}','py','special',2),
	 (6,'{"english": "fnmodule", "russian": "FN модуль", "deutsch": "FN Modul"}','py','common',5),
	 (7,'{"english": "config", "russian": "конфигурация", "deutsch": "Konfiguration"}','py','common',5),
	 (8,'{"english": "init", "russian": "инициализация", "deutsch": "initial"}','py','special',5),
	 (10,'{"english": "14tc10", "russian": "14тс-10", "deutsch": "14TC10"}','fnm','work',9),
	 (11,'{"english": "fake", "russian": "фальшивка", "deutsch": "gefälscht"}',NULL,'work',9),
	 (13,'{"english": "catalog", "russian": "каталог", "deutsch": "Katalog"}','txt','description',12),
	 (14,'{"english": "order", "russian": "заказ", "deutsch": "Auftrag"}','txt','description',12),
	 (16,'{"english": "time", "russian": "время", "deutsch": "Zeit"}','log',NULL,15);
INSERT INTO objects2 (id,name,"extension",innertype,parent_id) VALUES
	 (17,'{"english": "errors", "russian": "ошибки", "deutsch": "Fehler"}','log',NULL,15),
	 (1,'{"english": "service", "russian": "обслуживание", "deutsch": "Service"}',NULL,'node',0),
	 (2,'{"english": "commands", "russian": "команды", "deutsch": "Befehle"}',NULL,'node',1),
	 (5,'{"english": "core", "russian": "ядро", "deutsch": "Kern"}',NULL,'node',1),
	 (9,'{"english": "modules", "russian": "модули", "deutsch": "Module"}',NULL,'node',1),
	 (12,'{"english": "tasks", "russian": "задачи", "deutsch": "Aufgaben"}',NULL,'node',1),
	 (15,'{"english": "temp", "russian": "временный", "deutsch": "temp"}',NULL,'node',12);

ALTER SEQUENCE objects2_id_seq RESTART WITH 18;